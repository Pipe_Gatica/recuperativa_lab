#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import json


def load_file():

    with open("iris.json") as file:
        cargar = json.load(file)

    return cargar


def mostrar_especies(datos):

    lista =[]
    for i in datos:
        lista.append(i['species'])

    # Vamos a contar todas las veces que se repita cada elemento
    a = lista.count('setosa')
    b = lista.count('versicolor')
    c = lista.count('virginica')

    # Se eliminaran todos los elementos repetidos hasta que el elemento
    # llegue a 1, osea que ya no esta repetido
    while a > 1:
        lista.remove('setosa')
        a = lista.count('setosa')

    while b > 1:
        lista.remove('versicolor')
        b = lista.count('versicolor')

    while c > 1:
        lista.remove('virginica')
        c = lista.count('virginica')

    print('\nPrimera especie: ', lista[0])
    print('Segunda especie: ', lista[1])
    print('Tercera especie: ', lista[2])


def calcular_promedio(datos):

    lista1 = []
    lista2 = []
    lista3 = []
    lista4 = []
    lista5 = []
    lista6 = []

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=1 and i<51)
    for linea in leer: # lee las lineas correspondientes a la primera especie
        for x in linea:
            # agregamos los datos del largo de los petalos a una lista
            # para luego sumarlos y dividirlos para obtener el promedio
            lista1.append(x['petalLength']) # aqui va la longitud de los petalos
            promedio1 = sum(lista1) / 50

    # repetimos el procedimiento, pero para el ancho de los petalos
    for linea in leer:
        for x in linea:
            lista2.append(x['petalLength']) # aqui va la longitud de los petalos
            promedio2 = sum(lista2) / 50

    print('Setosas ---> Promedio de la longitud de sus petalos:', promedio1)
    print('        ---> Promedio del ancho de sus petalos', promedio2)

    # y ahora repetimos el procedimiento para las otras dos especies
    # me hubiera gustado hacer de esto una sola funcion y llamarla para cada
    # especie pero me complique mucho entregando los datos

    leer2 = (linea for i,linea in enumerate(f) if i>=51 and i<101)
    for linea in leer2:
        for x in linea:
            lista3.append(x['petalLength'])
            promedio3 = sum(lista3) / 50

    # repetimos el procedimiento, pero para el ancho de los petalos
    for linea in leer2:
        for x in linea:
            lista4.append(x['petalLength'])
            promedio4 = sum(lista4) / 50

    print('Versicolor ---> Promedio de la longitud de sus petalos:', promedio3)
    print('           ---> Promedio del ancho de sus petalos', promedio4)

    # una vez mas para la ultima especie
    leer3 = (linea for i,linea in enumerate(f) if i>=101 and i<151)
    for linea in leer3:
        for x in linea:
            lista5.append(x['petalLength'])
            promedio5 = sum(lista5) / 50

    # repetimos el procedimiento, pero para el ancho de los petalos
    for linea in leer3:
        for x in linea:
            lista6.append(x['petalLength'])
            promedio6 = sum(lista6) / 50

    print('Virginica ---> Promedio de la longitud de sus petalos:', promedio5)
    print('           ---> Promedio del ancho de sus petalos', promedio6)


def archivo_json(datos):

    # El error en esta funcion es que despues de guardar el archivo archivo json
    # cuando el bucle del for se va a ejecutar nuevamente pero en un x+1,
    # osea que la siguiente linea, no cargamos la informacion que tiene el
    # archivo que fue ingresado en el bucle anterior, despues de haber creado
    # el archivo. habia que cargarlo, ingresar informacion y guardar
    # y eso fue lo que me falto
    guardar_setona(datos)
    guardar_versicolor(datos)
    guardar_virginica(datos)
    print('\n¡Archivos creados exitosamente!')


def guardar_setona(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=1 and i<51)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("setosa.json", 'w') as file:
            json.dump(x, file)


def guardar_versicolor(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=51 and i<101)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("versicolor.json", 'w') as file:
            json.dump(x, file)

def guardar_virginica(datos):

    f = open("iris.json","r")
    leer = (linea for i,linea in enumerate(f) if i>=101 and i<151)

    for x in leer:
        #print(x)
        # con el print se arriba se ve que si separaba las 3 especies
        with open("virginica.json", 'w') as file:
            json.dump(x, file)


def main():

    datos = load_file()

    while True:
        print('\nPresione')
        print('1 para mostrar las especies')
        print('2 para calcular el promedio de alto y ancho de los petalos') # y el mas
        print('3 para mostrar cuantos registros de plantas se encuentran en el rango promedio')
        print('4 para buscar la medida y especie del sepalo mas alto')
        print('5 separar en archivos json cada especie')
        print('6 Cerrar programa')
        opt = int(input('Elija una opcion: '))

        if opt == 1:
            mostrar_especies(datos)
            pass

        elif opt == 2:
            # manda error en el segundo for de cada cilo en cual esperaba que,
            # se mostraran solo los datos del ancho o longitud, si se imprime
            # la variable linea de los primeros for,
            # se muestra bien las lineas que se piden
            calcular_promedio(datos)
            pass

        elif opt == 3: # Falta
            pass

        elif opt == 4: # Falta
            pass

        elif opt == 5:
            archivo_json(datos)
            pass

        elif opt == 6:
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass


if __name__ == '__main__':
    main()
